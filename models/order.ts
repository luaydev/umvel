import { model, Schema, Model, Document } from 'mongoose';

interface IOrder extends Document {
	orderItems:Schema.Types.ObjectId,
	subtotal: Number,
	vat: Number,
	total: Number,
	token:String,
	totalItems: Number,
	CustomerName: String,
	status: String
}

const OrderSchema: Schema = new Schema({
	orderItems: {
        type: Schema.Types.ObjectId,
        ref: 'orderItem',
    },
    subtotal: {
        type: Number,
        default: 0
    },
    vat: {
        type: Number,
        default: 0
    },
    total: {
        type: Number,
        default: 0
    },
    token: {
        type: String,
        default: 'token'
    },
    totalItems: {
        type: Number,
        default: 0
    },
    CustomerName : {
    	type: String,
    	default: 'cliente'
    },
	 status: {
        type: String,
        required: true,
        default: 'activo',
        emun: ['activo', 'cancelado', 'finalizado']
    },
})
/*
UserSchema.methods.toJSON = function() {
	const {name, ...usuario} = this.toObject();
	return usuario;
} */
export const Order: Model<IOrder> = model('Order', OrderSchema);




