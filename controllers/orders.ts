import { Request, Response } from 'express';
const { Order } = require('../models/order');
const { OrderItem } = require('../models/orderItem');

export const getOrders = async(req:Request, res: Response) => {
	const { limite= 5, desde = 0 } = req.query;
	const query = { estado: true };
	const [ total, orders] = await Promise.all([
			Order.countDocuments(query),
			Order.find(query)
				.skip( Number( desde ) )
            	.limit(Number( limite ))
		]); 
	res.json({total, orders})
}


export const getOrder = async(req:Request, res: Response) => {
	
	const {id} = req.params;
	const order = await Order.findById( id );		
	
	res.json({	
		order
		
	});
}




export const postOrder = async(req:Request, res: Response) => {
	
	const {orderItems, subtotal, vat, total, token, totalItems, CustomerName, status } = req.body;	
	const order = new Order({ subtotal, vat, total, token, totalItems, CustomerName, status })
	 await order.save(); 
	console.log(order._id)
	var idOrder = order._id
	orderItems.forEach(async (item : any )=> {
		console.log(item);
		var ordItem = new OrderItem({idOrder, item });
		await ordItem.save();
	})
	res.json({	
		order
	}); 
}



export const putOrder = (req:Request, res: Response) => {
	
	const { id } = req.params
	const {body} = req;	
	
	res.json({
		msg: 'putOrder',
		body
	});
} 

export const deleteOrder = async(req:Request, res: Response) => {
	
	const { id } = req.params
	// const usuarioEliminado = await User.findByIdAndUpdate(id, { estado: false })
	res.json({
		umsg: 'deleteOrder',
	});
}



