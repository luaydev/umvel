import { Router } from 'express';
import { getItems, getItem, postItem, putItem, deleteItem } from '../controllers/items';
const { check } = require('express-validator');
const { validarCampos } = require('../middlewares/validar');
const { existeOrderPorId } = require('../helpers/db-validators');

const router = Router();


router.get('/', getItems);
router.get('/:id', [
	check('id', 'No es un ID válido').isMongoId(),
	check('id').custom( existeOrderPorId )
	], getItem);

router.post('/',  postItem);

router.put('/:id',[
	check('id', 'No es un ID válido').isMongoId(),
	check('id').custom( existeOrderPorId )
	], putItem);

router.delete('/:id', [
	check('id', 'No es un ID válido').isMongoId(),
	check('id').custom( existeOrderPorId )
	], deleteItem);

export default router; 