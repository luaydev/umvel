import { Request, Response } from 'express';
const { OrderItem } = require('../models/orderItem');

export const getOrderItems = async(req:Request, res: Response) => {
	const { limite= 5, desde = 0 } = req.query;
	const query = { estado: true };
	const [ total, orderItems] = await Promise.all([
			OrderItem.countDocuments(query),
			OrderItem.find(query)
				.skip( Number( desde ) )
            	.limit(Number( limite ))
		]);
	res.json({total, orderItems})
}


export const getOrderItem = async(req:Request, res: Response) => {
	
	const {id} = req.params;
	const orderItem = await OrderItem.findById( id );		
	res.json({	
		orderItem
	});
}

export const postOrderItem = async(req:Request, res: Response) => {
	
	const { name, price } = req.body;	
	const orderItem = new OrderItem({name, price});
	await orderItem.save();
	res.json({
		orderItem
	});
}

export const putOrderItem = async(req:Request, res: Response) => {
	
	const { id } = req.params;
    const { name, price } = req.body;	
    const orderItem = await OrderItem.findByIdAndUpdate(id, {name, price}, { new: true });
    res.json( orderItem );
} 

export const deleteOrderItem = async(req:Request, res: Response) => {
	
	const { id } = req.params
	const itemEliminado = await OrderItem.findByIdAndUpdate(id, { estado: false })
	res.json({
		itemEliminado
	});
}



