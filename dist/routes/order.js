"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const orders_1 = require("../controllers/orders");
const { check } = require('express-validator');
const { validarCampos } = require('../middlewares/validar');
const { existeOrderPorId } = require('../helpers/db-validators');
const router = (0, express_1.Router)();
router.get('/', orders_1.getOrders);
router.get('/:id', [
    check('id', 'No es un ID válido').isMongoId(),
    check('id').custom(existeOrderPorId)
], orders_1.getOrder);
router.post('/', orders_1.postOrder);
router.put('/:id', [
    check('id', 'No es un ID válido').isMongoId(),
    check('id').custom(existeOrderPorId)
], orders_1.putOrder);
router.delete('/:id', [
    check('id', 'No es un ID válido').isMongoId(),
    check('id').custom(existeOrderPorId)
], orders_1.deleteOrder);
exports.default = router;
//# sourceMappingURL=order.js.map