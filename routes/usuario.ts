import { Router } from 'express';
import { getUsuarios, getUsuario, postUsuario, deleteUsuario } from '../controllers/usuarios';
const { check } = require('express-validator');
const { validarCampos } = require('../middlewares/validar');
const { existeUsuarioPorId } = require('../helpers/db-validators');

const router = Router();


router.get('/', getUsuarios);
router.get('/:id', [
	check('id', 'No es un ID válido').isMongoId(),
	check('id').custom( existeUsuarioPorId )
	], getUsuario);

router.post('/',  postUsuario);
/*
router.put('/:id',[
	check('id', 'No es un ID válido').isMongoId(),
	check('id').custom( existeUsuarioPorId )
	], putUsuario);
*/
router.delete('/:id', [
	check('id', 'No es un ID válido').isMongoId(),
	check('id').custom( existeUsuarioPorId )
	], deleteUsuario);

export default router; 