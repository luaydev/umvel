import { Request, Response } from 'express';
const { Item } = require('../models/item');

export const getItems = async(req:Request, res: Response) => {
	const { limite= 5, desde = 0 } = req.query;
	const query = { estado: true };
	const [ total, items] = await Promise.all([
			Item.countDocuments(query),
			Item.find(query)
				.skip( Number( desde ) )
            	.limit(Number( limite ))
		]);
	res.json({total, items})
}


export const getItem = async(req:Request, res: Response) => {
	
	const {id} = req.params;
	const item = await Item.findById( id );		
	res.json({	
		item
	});
}

export const postItem = async(req:Request, res: Response) => {
	
	const { name, price } = req.body;	
	const item = new Item({name, price});
	await item.save();
	res.json({
		item
	});
}

export const putItem = async(req:Request, res: Response) => {
	
	const { id } = req.params;
    const { name, price } = req.body;	
    const item = await Item.findByIdAndUpdate(id, {name, price}, { new: true });
    res.json( item );
} 

export const deleteItem = async(req:Request, res: Response) => {
	
	const { id } = req.params
	const itemEliminado = await Item.findByIdAndUpdate(id, { estado: false })
	res.json({
		itemEliminado
	});
}



