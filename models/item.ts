import { model, Schema, Model, Document } from 'mongoose';

interface IItem extends Document {
	name: String,
    price: Number,
    estado: Boolean
	
}

const ItemSchema: Schema = new Schema({
	name: {
        type: String,
        required: [true, 'El nombre es obligatorio'] 
    },
    price: {
        type: Number,
        default: 0
    },
    estado: {
        type: Boolean,
        default: true
    },
})
/*
UserSchema.methods.toJSON = function() {
	const {name, ...usuario} = this.toObject();
	return usuario;
} */
export const Item: Model<IItem> = model('Item', ItemSchema);




