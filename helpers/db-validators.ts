const { Usuario } = require('../models/usuario');
const { Order } = require('../models/order');

export const existeUsuarioPorId = async( id: String ) => {

    const existeUsuario = await Usuario.findById(id);
    if ( !existeUsuario ) {
        throw new Error(`El id no existe ${ id }`);
    }
}

export const existeOrderPorId = async( id: String ) => {

    const existeOrden = await Order.findById(id);
    if ( !existeOrden ) {
        throw new Error(`El id no existe ${ id }`);
    }
}