import { model, Schema, Model, Document } from 'mongoose';

interface IUser extends Document {
	name: String,
	estado: Boolean
}

const UserSchema: Schema = new Schema({
	name: { type: String, required: [true, 'El nombre es obligatorio'] },
	 estado: {
        type: Boolean,
        default: true
    },
})
/*
UserSchema.methods.toJSON = function() {
	const {name, ...usuario} = this.toObject();
	return usuario;
} */
export const User: Model<IUser> = model('User', UserSchema);