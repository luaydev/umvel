"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.deleteItem = exports.putItem = exports.postItem = exports.getItem = exports.getItems = void 0;
const { Item } = require('../models/item');
const getItems = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const { limite = 5, desde = 0 } = req.query;
    const query = { estado: true };
    const [total, items] = yield Promise.all([
        Item.countDocuments(query),
        Item.find(query)
            .skip(Number(desde))
            .limit(Number(limite))
    ]);
    res.json({ total, items });
});
exports.getItems = getItems;
const getItem = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const { id } = req.params;
    const item = yield Item.findById(id);
    res.json({
        item
    });
});
exports.getItem = getItem;
const postItem = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const { name, price } = req.body;
    const item = new Item({ name, price });
    yield item.save();
    res.json({
        item
    });
});
exports.postItem = postItem;
const putItem = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const { id } = req.params;
    const { name, price } = req.body;
    const item = yield Item.findByIdAndUpdate(id, { name, price }, { new: true });
    res.json(item);
});
exports.putItem = putItem;
const deleteItem = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const { id } = req.params;
    const itemEliminado = yield Item.findByIdAndUpdate(id, { estado: false });
    res.json({
        itemEliminado
    });
});
exports.deleteItem = deleteItem;
//# sourceMappingURL=items.js.map