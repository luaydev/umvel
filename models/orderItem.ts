import { model, Schema, Model, Document } from 'mongoose';

interface IOrderItem extends Document {
	order:Schema.Types.ObjectId,
    item:Schema.Types.ObjectId,
    estado: Boolean
	
}

const OrderItemSchema: Schema = new Schema({
	order: {
        type: Schema.Types.ObjectId,
        ref: 'Order',
        required: true
    },
    item: {
        type: Schema.Types.ObjectId,
        ref: 'Item',
        required: true
    },
    estado: {
        type: Boolean,
        default: true
    },
})
/*
UserSchema.methods.toJSON = function() {
	const {name, ...usuario} = this.toObject();
	return usuario;
} */
export const OrderItem: Model<IOrderItem> = model('OrderItem', OrderItemSchema);




