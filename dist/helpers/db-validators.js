"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.existeOrderPorId = exports.existeUsuarioPorId = void 0;
const { Usuario } = require('../models/usuario');
const { Order } = require('../models/order');
const existeUsuarioPorId = (id) => __awaiter(void 0, void 0, void 0, function* () {
    const existeUsuario = yield Usuario.findById(id);
    if (!existeUsuario) {
        throw new Error(`El id no existe ${id}`);
    }
});
exports.existeUsuarioPorId = existeUsuarioPorId;
const existeOrderPorId = (id) => __awaiter(void 0, void 0, void 0, function* () {
    const existeOrden = yield Order.findById(id);
    if (!existeOrden) {
        throw new Error(`El id no existe ${id}`);
    }
});
exports.existeOrderPorId = existeOrderPorId;
//# sourceMappingURL=db-validators.js.map