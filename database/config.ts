const mongoose = require('mongoose');

export const dbConnection = async() => {
	try {
		await mongoose.connect(process.env.MONGODB, {
			useNewUrlParser: true,
            useUnifiedTopology: true,
            // useCreateIndex: true,
            // useFindAndModify: false
		});
		console.log('BD online')
	} catch(error) {
		console.log(error);
		throw new Error('Error al conectarse a la BD')
	}
}
