"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.User = void 0;
const mongoose_1 = require("mongoose");
const UserSchema = new mongoose_1.Schema({
    name: { type: String, required: [true, 'El nombre es obligatorio'] },
    estado: {
        type: Boolean,
        default: true
    },
});
/*
UserSchema.methods.toJSON = function() {
    const {name, ...usuario} = this.toObject();
    return usuario;
} */
exports.User = (0, mongoose_1.model)('User', UserSchema);
//# sourceMappingURL=usuario.js.map