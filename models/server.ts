import express, {Application} from 'express';
import userRoutes from '../routes/usuario';
import orderRoutes from '../routes/order';
import itemsRoutes from '../routes/item';
import cors from 'cors';
const {dbConnection} = require('../database/config');

class Server {

	private app: Application;
	private port:string;
	private apiPaths = {
		usuarios: '/api/usuarios',
		orders: '/api/ordenes',
		items: '/api/items'
	}

	constructor(){
		this.app = express();
		this.port = process.env.PORT || '8000';
		this.conectarBD();
		this.middlewares();
		this.routes();


	}

	async conectarBD(){
		await dbConnection();
	}

	routes() {
		this.app.use(this.apiPaths.usuarios, userRoutes)
		this.app.use(this.apiPaths.orders, orderRoutes)
		this.app.use(this.apiPaths.items, itemsRoutes)
	}

	middlewares(){
		//cors
		this.app.use(cors())

		//lectura de body
		this.app.use(express.json())

		//carpeta publica
		this.app.use(express.static('public'))
	}

	listen(){
		this.app.listen(this.port, () => {
			console.log(`Servidor corriendo en puerto ${this.port}`);
		})
	}
}

export default Server;