"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const usuarios_1 = require("../controllers/usuarios");
const { check } = require('express-validator');
const { validarCampos } = require('../middlewares/validar');
const { existeUsuarioPorId } = require('../helpers/db-validators');
const router = (0, express_1.Router)();
router.get('/', usuarios_1.getUsuarios);
router.get('/:id', [
    check('id', 'No es un ID válido').isMongoId(),
    check('id').custom(existeUsuarioPorId)
], usuarios_1.getUsuario);
router.post('/', usuarios_1.postUsuario);
/*
router.put('/:id',[
    check('id', 'No es un ID válido').isMongoId(),
    check('id').custom( existeUsuarioPorId )
    ], putUsuario);
*/
router.delete('/:id', [
    check('id', 'No es un ID válido').isMongoId(),
    check('id').custom(existeUsuarioPorId)
], usuarios_1.deleteUsuario);
exports.default = router;
//# sourceMappingURL=usuario.js.map