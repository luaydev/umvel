import { Request, Response } from 'express';
const { User } = require('../models/usuario');

export const getUsuarios = async(req:Request, res: Response) => {
	const { limite= 5, desde = 0 } = req.query;
	const query = { estado: true };
	const [ total, usuarios] = await Promise.all([
			User.countDocuments(query),
			User.find(query)
				.skip( Number( desde ) )
            	.limit(Number( limite ))
		]);
	res.json({total, usuarios})
}


export const getUsuario = async(req:Request, res: Response) => {
	
	const {id} = req.params;
	const usuario = await User.findById( id );		

	res.json({	
		msg: 'getUsuarios',
		usuario
	});
}




export const postUsuario = async(req:Request, res: Response) => {
	
	const { name } = req.body;	
	const usuario = new User({name});
	await usuario.save();
	res.json({
		usuario
	});
}

/*

export const putUsuario = (req:Request, res: Response) => {
	
	const { id } = req.params
	const {body} = req;	

	res.json({
		msg: 'putUsuarios',
		body
	});
} */

export const deleteUsuario = async(req:Request, res: Response) => {
	
	const { id } = req.params
	const usuarioEliminado = await User.findByIdAndUpdate(id, { estado: false })
	res.json({
		usuarioEliminado
	});
}



