"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.deleteOrderItem = exports.putOrderItem = exports.postOrderItem = exports.getOrderItem = exports.getOrderItems = void 0;
const { OrderItem } = require('../models/orderItem');
const getOrderItems = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const { limite = 5, desde = 0 } = req.query;
    const query = { estado: true };
    const [total, orderItems] = yield Promise.all([
        OrderItem.countDocuments(query),
        OrderItem.find(query)
            .skip(Number(desde))
            .limit(Number(limite))
    ]);
    res.json({ total, orderItems });
});
exports.getOrderItems = getOrderItems;
const getOrderItem = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const { id } = req.params;
    const orderItem = yield OrderItem.findById(id);
    res.json({
        orderItem
    });
});
exports.getOrderItem = getOrderItem;
const postOrderItem = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const { name, price } = req.body;
    const orderItem = new OrderItem({ name, price });
    yield orderItem.save();
    res.json({
        orderItem
    });
});
exports.postOrderItem = postOrderItem;
const putOrderItem = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const { id } = req.params;
    const { name, price } = req.body;
    const orderItem = yield OrderItem.findByIdAndUpdate(id, { name, price }, { new: true });
    res.json(orderItem);
});
exports.putOrderItem = putOrderItem;
const deleteOrderItem = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const { id } = req.params;
    const itemEliminado = yield OrderItem.findByIdAndUpdate(id, { estado: false });
    res.json({
        itemEliminado
    });
});
exports.deleteOrderItem = deleteOrderItem;
//# sourceMappingURL=orderItems.js.map