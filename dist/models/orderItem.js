"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.OrderItem = void 0;
const mongoose_1 = require("mongoose");
const OrderItemSchema = new mongoose_1.Schema({
    order: {
        type: mongoose_1.Schema.Types.ObjectId,
        ref: 'Order',
        required: true
    },
    item: {
        type: mongoose_1.Schema.Types.ObjectId,
        ref: 'Item',
        required: true
    },
    estado: {
        type: Boolean,
        default: true
    },
});
/*
UserSchema.methods.toJSON = function() {
    const {name, ...usuario} = this.toObject();
    return usuario;
} */
exports.OrderItem = (0, mongoose_1.model)('OrderItem', OrderItemSchema);
//# sourceMappingURL=orderItem.js.map