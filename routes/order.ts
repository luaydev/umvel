import { Router } from 'express';
import { getOrders, getOrder, postOrder, putOrder, deleteOrder } from '../controllers/orders';
const { check } = require('express-validator');
const { validarCampos } = require('../middlewares/validar');
const { existeOrderPorId } = require('../helpers/db-validators');

const router = Router();


router.get('/', getOrders);
router.get('/:id', [
	check('id', 'No es un ID válido').isMongoId(),
	check('id').custom( existeOrderPorId )
	], getOrder);

router.post('/',  postOrder);

router.put('/:id',[
	check('id', 'No es un ID válido').isMongoId(),
	check('id').custom( existeOrderPorId )
	], putOrder);

router.delete('/:id', [
	check('id', 'No es un ID válido').isMongoId(),
	check('id').custom( existeOrderPorId )
	], deleteOrder);

export default router; 