"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Item = void 0;
const mongoose_1 = require("mongoose");
const ItemSchema = new mongoose_1.Schema({
    name: {
        type: String,
        required: [true, 'El nombre es obligatorio']
    },
    price: {
        type: Number,
        default: 0
    },
    estado: {
        type: Boolean,
        default: true
    },
});
/*
UserSchema.methods.toJSON = function() {
    const {name, ...usuario} = this.toObject();
    return usuario;
} */
exports.Item = (0, mongoose_1.model)('Item', ItemSchema);
//# sourceMappingURL=item.js.map