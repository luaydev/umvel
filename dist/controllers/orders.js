"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.deleteOrder = exports.putOrder = exports.postOrder = exports.getOrder = exports.getOrders = void 0;
const { Order } = require('../models/order');
const { OrderItem } = require('../models/orderItem');
const getOrders = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const { limite = 5, desde = 0 } = req.query;
    const query = { estado: true };
    const [total, orders] = yield Promise.all([
        Order.countDocuments(query),
        Order.find(query)
            .skip(Number(desde))
            .limit(Number(limite))
    ]);
    res.json({ total, orders });
});
exports.getOrders = getOrders;
const getOrder = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const { id } = req.params;
    const order = yield Order.findById(id);
    res.json({
        order
    });
});
exports.getOrder = getOrder;
const postOrder = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const { orderItems, subtotal, vat, total, token, totalItems, CustomerName, status } = req.body;
    const order = new Order({ subtotal, vat, total, token, totalItems, CustomerName, status });
    yield order.save();
    console.log(order._id);
    var idOrder = order._id;
    orderItems.forEach((item) => __awaiter(void 0, void 0, void 0, function* () {
        console.log(item);
        var ordItem = new OrderItem({ idOrder, item });
        yield ordItem.save();
    }));
    res.json({
        order
    });
});
exports.postOrder = postOrder;
const putOrder = (req, res) => {
    const { id } = req.params;
    const { body } = req;
    res.json({
        msg: 'putOrder',
        body
    });
};
exports.putOrder = putOrder;
const deleteOrder = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const { id } = req.params;
    // const usuarioEliminado = await User.findByIdAndUpdate(id, { estado: false })
    res.json({
        umsg: 'deleteOrder',
    });
});
exports.deleteOrder = deleteOrder;
//# sourceMappingURL=orders.js.map