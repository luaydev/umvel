"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Order = void 0;
const mongoose_1 = require("mongoose");
const OrderSchema = new mongoose_1.Schema({
    orderItems: {
        type: mongoose_1.Schema.Types.ObjectId,
        ref: 'orderItem',
    },
    subtotal: {
        type: Number,
        default: 0
    },
    vat: {
        type: Number,
        default: 0
    },
    total: {
        type: Number,
        default: 0
    },
    token: {
        type: String,
        default: 'token'
    },
    totalItems: {
        type: Number,
        default: 0
    },
    CustomerName: {
        type: String,
        default: 'cliente'
    },
    status: {
        type: String,
        required: true,
        default: 'activo',
        emun: ['activo', 'cancelado', 'finalizado']
    },
});
/*
UserSchema.methods.toJSON = function() {
    const {name, ...usuario} = this.toObject();
    return usuario;
} */
exports.Order = (0, mongoose_1.model)('Order', OrderSchema);
//# sourceMappingURL=order.js.map