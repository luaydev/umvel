"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const items_1 = require("../controllers/items");
const { check } = require('express-validator');
const { validarCampos } = require('../middlewares/validar');
const { existeOrderPorId } = require('../helpers/db-validators');
const router = (0, express_1.Router)();
router.get('/', items_1.getItems);
router.get('/:id', [
    check('id', 'No es un ID válido').isMongoId(),
    check('id').custom(existeOrderPorId)
], items_1.getItem);
router.post('/', items_1.postItem);
router.put('/:id', [
    check('id', 'No es un ID válido').isMongoId(),
    check('id').custom(existeOrderPorId)
], items_1.putItem);
router.delete('/:id', [
    check('id', 'No es un ID válido').isMongoId(),
    check('id').custom(existeOrderPorId)
], items_1.deleteItem);
exports.default = router;
//# sourceMappingURL=item.js.map